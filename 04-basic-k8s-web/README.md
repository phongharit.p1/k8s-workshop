`kubectl port-forward <pods> 8080:8080`

stop deployment
kubectl scale basic-k8s-deployment --replicas=0

delete deployment
kubectl delete basic-k8s-deployment
