# Basic Docker

Login
`docker login`

build your docker:
`docker build -t <image-name>:<tag-version> .`
Note: tag name should be ths same in `docker-compose.yml` at image: <image-name>:<tag-version>, need to change follow by tag name

run docker
`docker run -p <container-port>:<host-port> <image-name>` or `docker run -p -d  <container-port>:<host-port> <image-name>`

run docker (compose) in directory with docker-compose.yaml
`docker-compose up` or `docker-compose up -d`

remove container, network, volumes
`docker-compose down`

access via
`http://localhost:8080/index.html`

# push our images

open https://hub.docker.com/ and login

get your username e.g. `aaa`

push your image via
`docker push <username>/<image-name>:<tag-version>`

# Pull images

`docker pull <image>`

get images list:
`docker images`

or filer
`docker images | grep <name>`

remove image
`docker rmi <image-name>:<tag-name>`

remove container
`docker rm <image-name>:<tag-name>`

get container list
`docker ps`

stop container
`docker stop <container-id>`

remove container by id
`docker rm <container-id>`
